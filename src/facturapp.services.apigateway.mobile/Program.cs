using System;
using System.Linq;
using Convey;
using Convey.Logging;
using Convey.Metrics.AppMetrics;
using Convey.Security;
using facturapp.services.apigateway.mobile.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ntrada;
using Ntrada.Extensions.RabbitMq;
using Ntrada.Hooks;

{
    var builder = WebApplication.CreateBuilder(args);
    
    const string extension = "yml";
    var ntradaConfig = Environment.GetEnvironmentVariable("NTRADA_CONFIG");
    var configPath = args?.FirstOrDefault() ?? ntradaConfig ?? $"ntrada.{extension}";
    
    if (!configPath.EndsWith($".{extension}")) configPath += $".{extension}";
    
    builder.Configuration.AddYamlFile(configPath, false);
    
    builder.Services
        .AddNtrada()
        .AddSingleton<IContextBuilder, CorrelationContextBuilder>()
        .AddSingleton<ISpanContextBuilder, SpanContextBuilder>()
        .AddSingleton<IHttpRequestHook, HttpRequestHook>()
        .AddConvey()
        .AddMetrics()
        .AddSecurity();
    
    builder.Host.UseLogging();
    
    var app = builder.Build();
    
    app.UseNtrada();
    app.Run();
}