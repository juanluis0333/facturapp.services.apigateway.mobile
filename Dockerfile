FROM mcr.microsoft.com/dotnet/sdk:5.0.202 AS build
WORKDIR /app
COPY . .
RUN dotnet publish src/facturapp.services.apigateway.mobile -c release -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app/out .

COPY --from=build /app/src/facturapp.services.apigateway.mobile/ntrada.yml .
COPY --from=build /app/src/facturapp.services.apigateway.mobile/ntrada.dev.yml .
COPY --from=build /app/src/facturapp.services.apigateway.mobile/ntrada.docker.yml .
COPY --from=build /app/src/facturapp.services.apigateway.mobile/ntrada-async.yml .
COPY --from=build /app/src/facturapp.services.apigateway.mobile/ntrada-async.docker.yml .

ENV ASPNETCORE_URLS http://*:80
ENV ASPNETCORE_ENVIRONMENT Development
ENV NTRADA_CONFIG ntrada.dev
ENTRYPOINT dotnet facturapp.services.apigateway.mobile.dll